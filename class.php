<?php

namespace FileReadFactory;

/**
 * Factory
 */
abstract class FilePrint
{
    protected $file, $fileData;

    public function __construct(string $file)
    {
        $this->file = $file;
    }

    public function setFileData(array $filterFields): void
    {
        $this->read();
        $this->prepare();
        $this->fileData = $this->get($filterFields);
    }

    public function get($filterFields): array
    {
        $this->fileData = $this->filter($this->fileData, $filterFields);
        return $this->fileData;
    }

    public function getFileData(array $filterFields): array
    {
        return $this->fileData;
    }

    public function print(): void
    {
        $output = "<table class='table'>";
        foreach($this->fileData as $row )
        {
            $output .= "<tr><td>";
            $output .= implode("</td><td>", $row);
            $output .= "</td></tr>";
        }
        $output .= "</table>";
        echo $output;
    }

/**
* Read file to array
*/
    abstract public function read(): void;

/**
* Normalaize output array Bring it to the [0] => field names then output data
* No empty rows
*/
    abstract public function prepare(): void;

/**
* Filter needed fields by F name before output
*/

    public function removeEmptyLines($arr)
    {
        return array_values(array_filter($arr, fn($row) =>  !empty(array_filter($row))));
    }

    public function filter($arr, $filterFields)
    {
        $indexLine = $arr[0];
        $neededKeys = Array();
        $finalArr = Array();
        foreach($filterFields as $field)
        {
            $temp = array_keys($indexLine, $field);
            if(!empty($temp)){
                $neededKeys = array_merge ($neededKeys, $temp);
            }
        }
        foreach($arr as $num => $row)
        {
            foreach($row as $elnum => $el)
            {
                if(in_array($elnum, $neededKeys)){
                    $finalArr[$num][$elnum] = $el;
                }
            }
        }
        return $finalArr;
    }

    public function convertJsonArray($arr)
    {
        $tempArr = Array();
        foreach($arr as $num => $row )
        {
            if($num == 0){
                foreach($row as $name => $cell){
                    $tempArr[0][] = $name;
                }
            }
            $tempArr[] = array_values((array) $row);
        }
        return $tempArr;
    }

}

class JsonReader extends FilePrint
{
    public function read(): void
    {
        $json = file_get_contents($this->file);
        $this->fileData = json_decode($json);
    }

    public function prepare(): void
    {
        $this->fileData = $this->convertJsonArray($this->fileData);
        $this->fileData = $this->removeEmptyLines($this->fileData);
    }
}

class CsvReader extends FilePrint
{
    public function read(): void
    {
        $this->fileData = array_map('str_getcsv', file($this->file));
    }

    public function prepare(): void
    {
        $this->fileData = $this->removeEmptyLines($this->fileData);
    }

}

class XmlReader extends FilePrint
{
    public function read(): void
    {
        $xmlstring = file_get_contents($this->file);
        $this->fileData = json_decode(json_encode((array) simplexml_load_string($xmlstring, "SimpleXMLElement", LIBXML_NOCDATA)), true);
    }

    public function prepare(): void
    {
        $tempArr = Array();
        foreach($this->fileData['Record'] as $num => $row)
        {
            $tempArr[$num] = array_values($row['Row']['@attributes']);

        }
        $this->fileData = $tempArr;
        $this->fileData = $this->removeEmptyLines($this->fileData);
    }
}
